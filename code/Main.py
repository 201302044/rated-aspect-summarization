import math
import csv
import random
import nltk
import numpy as np
import re
import operator
import sys

if(sys.argv[1] == 'camera' and sys.argv[2] == 'plsa'):
    import camera_PLSA as plsa

elif(sys.argv[1] == 'camera' and sys.argv[2] == 'lda'):
    import camera_LDA as LDA

elif(sys.argv[1] == 'computer' and sys.argv[2] == 'plsa'):
    import computer_PLSA as plsa

elif(sys.argv[1] == 'computer' and sys.argv[2] == 'lda'):
    import computer_LDA as LDA

elif(sys.argv[1] == 'phone' and sys.argv[2] == 'plsa'):
    import phone_PLSA as plsa

elif(sys.argv[1] == 'phone' and sys.argv[2] == 'lda'):
    import phone_LDA as LDA

elif(sys.argv[1] == 'tv' and sys.argv[2] == 'plsa'):
    import tv_PLSA as plsa

elif(sys.argv[1] == 'tv' and sys.argv[2] == 'lda'):
    import tv_LDA as LDA

#import Aspects

Count = {}

def Load():
    f = open('reviews_Electronics.csv', 'rb')
    lines = csv.reader(f)
    temp = list(lines)
    Data = []
    for x in range(500): # Change limit accordingly
        l = random.randint(1, 1000)
        temp[l][0] = int(temp[l][0])
        #temp[x][1] = list(temp[x][1])
        temp[l][3] = float(temp[l][3])
        Data.append(temp[l])
    f.close()
    f = open('./outmat1.txt')
    lines = f.read()
    f.close()
    lines = lines.split('\n')
    Data = []
    for i in lines:
        temp = i.split(' ')
        if(len(temp) == 2):
            Data.append([0, 0, temp[1], 0])
    return Data

def Load_Phrases1():
    f = open('phrases_both.txt', 'r')
    x = f.read();
    x = x.split('\n')
    Res = ''
    for i in x:
        temp = i
        temp = temp.split(',')
        res = []
        for j in temp:
            res.append(j.strip(' '))
        #print res
        try:
            Res += res[0] + ' ' + res[1] + ' '
        except Exception:
            pass
    f.close()
    return Res

def Text(Data):
    S = ''
    for i in Data:
        inspace = ["[","]","?","(",")","!","{","}","<",">", ";","=","|","^","."]
        t = i[2]
        for pun in inspace:
            if pun in t:
                t = t.replace(pun," " + pun + " ")

        S += t
        S += ' '
    return S

def normalize(vec):
    s = sum(vec)
    for i in range(len(vec)):
        vec[i] = vec[i] * 1.0 / s

def PLSA(Data):
    #S = Text(Data)
    S = Data
    ttokens = nltk.word_tokenize(S)
    #ttokens = Tokenize(S)
    for j in xrange(len(ttokens)):
        ttokens[j] = ttokens[j].lower()
    Terms = []
    tokens = []
    f = open('./stopwords.txt')
    x = f.read()
    x = x.split('\n')
    print len(ttokens)
    Punct = ['.', '"', ',', 'i', '#', '$', '(', ')', ':', ';', ',', '-', '!', '.', '?', '/', '"', '*', '&']
    Total = 0
    for i in ttokens:
        if(i in x or i in Punct):
            continue
        else:
            tokens.append(i)
            Total += 1

    for i in tokens:
        try:
            Count[i] += 1
        except KeyError:
            Count[i] = 1
            Terms.append(i)

    T = len(Terms) # Number of terms
    print T
    N = 1 # Number of documents
    K = 4 # Number of topics

    document_topic_prob = np.zeros([N, K], dtype=np.float) # P(z | d)
    topic_word_prob = np.zeros([K, T], dtype=np.float) # P(w | z)
    topic_prob = np.zeros([N, T, K], dtype=np.float) # P(z | d, w)
    document_topic_prob = np.random.random(size = (N, K))

    for d_index in range(N):
        normalize(document_topic_prob[d_index]) # normalize for each document
    topic_word_prob = np.random.random(size = (K, T))
    for z in range(K):
        normalize(topic_word_prob[z]) # normalize for each topic

    epoh = 1
    while(epoh <= 50):
        print epoh
        for w_index in range(T):
            prob = document_topic_prob[0, :] * topic_word_prob[:, w_index]
            if sum(prob) == 0.0:
                exit(0)
            else:
                normalize(prob)
            topic_prob[0][w_index] = prob
        for z in range(K):
            for w_index in range(T):
                s = 0
                for d_index in range(N):
                    count = Count[Terms[w_index]]
                    s = s + count * topic_prob[d_index, w_index, z]
                topic_word_prob[z][w_index] = s
            normalize(topic_word_prob[z])

        for d_index in range(N):
            for z in range(K):
                s = 0
                for w_index in range(T):
                    count = Count[Terms[w_index]]
                    s = s + count * topic_prob[d_index, w_index, z]
                document_topic_prob[d_index][z] = s
            normalize(document_topic_prob[d_index])
        epoh += 1

    temp = {}
    for i in xrange(K):
        temp[i] = []
        for j in xrange(T):
            temp[i].append([topic_word_prob[i][j], Terms[j]])
        temp[i].sort(reverse = True)
        print temp[i][::30]
        print "----------------------------------"

    return None    

def LDAParse():
    LD = LDA.LD
    Ans = []
    for i in LD:
        temp = i
        temp = temp.split(' + ')
        tans = []
        for j in temp:
            tans.append(j.split('*')[1])
        Ans.append(tans)
    Aspects = []
    for i in Ans:
        temp = []
        for j in i:
            temp.append(str(j))
        Aspects.append(temp)
    #print Aspects
    #exit(0)
    return Aspects

def PLSAParse():
    LD = plsa.plsa
    Aspects = []
    for i in LD:
        temp = []
        for j in i:
            temp.append(j[0])
        Aspects.append(temp)
    return Aspects

def Load_Phrases():

    f = open('phrases_both_'+sys.argv[1]+'.txt', 'r')
    x = f.read();
    x = x.split('\n')
    Res = []
    for i in x:
        temp = i
        temp = temp.split(',')
        res = []
        for j in temp:
            res.append(j.strip(' '))
        try:
            res[2] = int(res[2])
            res[3] = int(res[3])
            Res.append(res)
        except Exception:
            pass
    f.close()
    return Res

def GlobalPrediction(Aspects, Type):
    if(Type == 0):
        r = 2
    else:
        r = 3
    Data = Load_Phrases()
    #print Aspects
    #print Data[0]
    Prob = {}
    Ans = []
    for i in xrange(20):
        temp = []
        for j in xrange(20):
            temp.append([('', 0.0, 0.0)])
        Ans.append(temp)

    p = 0
    #print Data[0:5]
    #print 
    #print 
    #print 
    #print Aspects
    for asp in Aspects:
        q = 0
        for a in asp:
            Prob = {}
            Count = {}
            for rating in xrange(1, 6):
                Modifiers = []
                for i in Data:
                    #if(a.lower() == 'lenses' and i[1].lower() == 'lenses'):
                    #    print "YES"
                    if(a.lower() in i[1].lower() and rating == i[r]):
                        #print rating, a.lower(), i[1].lower(), i[r]
                        try:
                            Count[ (i[0].lower(), a.lower(), rating) ] += 1
                        except KeyError:
                            Count[ (i[0].lower(), a.lower(), rating) ] = 1
                        if(i[0].lower() not in Modifiers):
                            Modifiers.append(i[0].lower())
                Sum = 0
                for i in Modifiers:
                    Sum += Count[ (i.lower(), a.lower(), rating) ]
                for i in Modifiers:
                    Prob[ (i.lower(), a.lower(), rating) ] = (Count[ (i.lower(), a.lower(), rating) ] + 0.00) / (Sum + 0.00)
            temp = sorted(Prob.items(), key=operator.itemgetter(1))
            temp = temp[::-1]
            #print temp
            try:
                Ans[p][q] = [ (temp[0][0][0], temp[0][0][2], Count[ (temp[0][0][0], temp[0][0][1], temp[0][0][2]) ]) ]
            except Exception:
                pass
            q += 1
        Sum = 0.00
        Count = 0
        #print Ans[p]
        #print p, Ans[p]
        for o in Ans[p]:
            if(o[0][1] > 0):
                Sum += o[0][1]
                Count += 1
        #print asp
        print p, Sum, Count, round((Sum / (Count + 0.00)), 2)
        p += 1
    return Ans

def ReprPhrases(Aspects, Distr):
    #Aspects = [['use', 'digit', 'game', 'can', 'easy', 'shot', 'play', 'make', 'web', 'pc', 'great', 'software', 'e', 'computer', 'also', 'read', 'machine', 'fun', 'love', 'reader']]
    #Distr = [[[('other', 2, 12)], [('first', 5, 39)], [('free', 2, 3)], [('good', 2, 4)], [('many', 3, 2)], [('good', 1, 2)], [('great', 4, 49)], [('and/or', 3, 2)], [('good', 2, 3)], [('better', 2, 3)], [('horrible', 1, 2)], [('easy', 5, 45)], [('great', 5, 5167)], [('other', 1, 15)], [('regular', 1, 2)], [('other', 2, 8)], [('good', 2, 4)], [('full', 1, 29)], [('first', 1, 2)], [('great', 2, 2)]]]
    Results = []
    #print len(Aspects)
    #print Aspects
    #print len(Distr)
    #print len(Distr[0])
    #print Distr
    for i in xrange(len(Distr)):
        temp = []
        a = []
        for j in xrange(len(Distr[i])):
            #print i, j, Distr[i][j], len(Aspects), len(Aspects[i]), Aspects[i][j]
            a.append([Distr[i][j][0][2], Distr[i][j][0][0], Aspects[i][j]])
        a.sort(reverse = True)
        for j in xrange(8):
            try:
                temp.append(a[j])
            except IndexError:
                pass
        Results.append(temp)
    return Results

#Data = Load_Phrases1()
#print Data
#PLSA(Data)
#exit(0)
#Aspects = LDAParse()
if(sys.argv[2] == 'plsa'):
    Aspects = PLSAParse()
else:
    Aspects = LDAParse()
#print Aspects
Results = []

Results = GlobalPrediction(Aspects, 0)
Results = ReprPhrases(Aspects, Results)
for i in Results:
    print i
print
Results = GlobalPrediction(Aspects, 1)
Results = ReprPhrases(Aspects, Results)
for i in Results:
    print i