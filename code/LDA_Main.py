import csv
import random
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim

def Load():
    f = open('out_phone.csv', 'rb')
    lines = csv.reader(f)
    temp = list(lines)
    Data = []
    for x in xrange(len(temp)):
        Data.append(temp[x])
    f.close()
    return Data

def LDA():
    tokenizer = RegexpTokenizer(r'\w+')
    en_stop = get_stop_words('en')
    p_stemmer = PorterStemmer()

    doc_set = []
    Data = Load()
    for i in Data:
        doc_set.append(i[2])
    texts = []
    for i in doc_set:
        raw = i.lower()
        tokens = tokenizer.tokenize(raw)
        stopped_tokens = [i for i in tokens if not i in en_stop]
        stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]
        texts.append(stemmed_tokens)
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=20, id2word = dictionary, passes=20)
    print(ldamodel.print_topics(num_topics=20, num_words=20))
LDA()
