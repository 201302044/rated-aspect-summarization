import csv
from scipy.interpolate import interp1d
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import gaussian_kde

maxi  = 0.875
mini = -0.625
def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)
vals = []
m = interp1d([mini,maxi],[1,5])
with open('out_1.csv', 'rb') as f:
	reader = csv.reader(f)
	for row in reader:
		temp = float(row[4])
		#print row[3], m(temp)
		vals.append(temp)
		#print translate(temp,mini,maxi,1,5)
#print len(vals)
'''vals.sort()
density = gaussian_kde(vals)
xs = np.linspace(-1,1,200)
density.covariance_factor = lambda : .0001
density._compute_covariance()
plt.plot(xs,density(xs))
plt.show()
'''
a = np.array(vals)

print len(a)
c = np.where(np.logical_and(a>=-0.08, a<=0.12))
print len(c[0].tolist())

for i in vals:
	if translate(i,-0.08,0.12,1,5) < 1.0:
		print "1.0"
	elif translate(i,-0.08,0.12,1,5) > 5.0:
		print "5.0"
	else:
		print round(translate(i,-0.08,0.12,1,5))
	#pass
