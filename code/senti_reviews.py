import csv,nltk
'''score_dict = {}
a = open("SentiWordNet_scores_final.txt","r")
for i in a.readlines():
	x, y = i.split('^')
	score_dict[x] = y.split('\n')[0]

print "scores_dict =" , score_dict 
'''
from sentiscores_dict import scores_dict

inspace = ["[","]","?","(",")","!","{","}","<",">", ";","=","|","^","."]


with open('out_phone.csv', 'rb') as f:
	reader = csv.reader(f)
	for row in reader:
		temp = row[2]
		#temp = "Didn't work, had to send back"
		for pun in inspace:
			if pun in temp:
				temp = temp.replace(pun," " + pun + " ")
		text = nltk.word_tokenize(temp)
		tagged = nltk.pos_tag(text)
		#print tagged
		temp_sentence  = ""
		for i in tagged:
			if not (i[1]== 'DT' or i[1] == 'IN' or i[1] == 'PRP' or i[1] == 'PRP$' or i[1] == 'TO' ):
				temp_sentence += i[0].lower() + "_" + i[1] + " " 
		#print temp_sentence + '\n'
		temp_sentence = temp_sentence.replace("_NNP", "_n");
		temp_sentence = temp_sentence.replace("_NNS", "_n");
		temp_sentence = temp_sentence.replace("_NN", "_n");
		temp_sentence = temp_sentence.replace("_RBR", "_r");
		temp_sentence = temp_sentence.replace("_RBS", "_r");
		temp_sentence = temp_sentence.replace("_RB", "_r");
		temp_sentence = temp_sentence.replace("_JJR", "_a");
		temp_sentence = temp_sentence.replace("_JJS", "_a");
		temp_sentence = temp_sentence.replace("_JJ", "_a");
		temp_sentence = temp_sentence.replace("_VBD", "_v");
		temp_sentence = temp_sentence.replace("_VBG", "_v");
		temp_sentence = temp_sentence.replace("_VBN", "_v");
		temp_sentence = temp_sentence.replace("_VBP", "_v");
		temp_sentence = temp_sentence.replace("_VBZ", "_v");
		temp_sentence = temp_sentence.replace("_VB", "_v")
		#print temp_sentence
		af_message = nltk.word_tokenize(temp_sentence)
		score =0.0
		sscore = 0.0
		count = 0 
		for word in af_message:
			if word in scores_dict:
				#print word, scores_dict[word]
				score += float(scores_dict[word])
				count += 1
		if count >= 1:
			sscore = score/count

		ofile  = open("out_fphone.csv", "a+b")
		c = csv.writer(ofile)

		c.writerow([row[0],row[1],row[2],row[3],sscore,count])
		ofile.close()
				

		
