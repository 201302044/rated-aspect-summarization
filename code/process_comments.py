import csv,nltk,operator
from textblob import TextBlob
inspace = ["[","]","?","(",")","!","{","}","<",">", ";","=","|","^","."]
freq_nn = {}

with open('out_fphone.csv', 'rb') as f:
	reader = csv.reader(f)
	for row in reader:
		temp =row[2]
		for pun in inspace:
			if pun in temp:
				temp = temp.replace(pun," " + pun + " ")
		text = nltk.word_tokenize(temp)
		tagged = nltk.pos_tag(text)
		used = [0]*len(tagged)
		#print tagged
		flag= 0
		for i in xrange(0,len(tagged)):
			if tagged[i][1] == 'NN' or tagged[i][1] == 'NNP' or tagged[i][1] == 'NNS' or tagged[i][1] == 'NNPS':
				#print tagged[i]
				flag=0
				
				for back in xrange(1,6):
					if i-back < 0:
						break
					if (tagged[i-back][1] == 'JJ' or tagged[i-back][1] == 'JJR' or tagged[i-back][1] == 'JJS')and used[i-back] == 0:
						print tagged[i-back][0], ',', tagged[i][0], ',', row[3], ',',row[6]
						used[i-back] = 1
						flag =1
						try:
							freq_nn[tagged[i][0]] += 1
						except KeyError:
							freq_nn[tagged[i][0]] = 1
						break	
				for back in xrange(1,6):
					if (i+back > len(tagged)-1) or flag == 1:
						break
					if (tagged[i+back][1] == 'JJ' or tagged[i+back][1] == 'JJR' or tagged[i+back][1] == 'JJS')and used[i+back] == 0:
						print tagged[i+back][0],',',tagged[i][0],',', row[3], ',',row[6]
						used[i+back] = 1
						flag =1
						try:
							freq_nn[tagged[i][0]] += 1
						except KeyError:
							freq_nn[tagged[i][0]] = 1
						break		
		if int(row[0]) > 50000 :
			break
		